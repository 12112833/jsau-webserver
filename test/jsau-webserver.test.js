'use strict'
const app = require('../src/jsau-webserver')
const request = require('supertest')

describe('test webserver', () => {
    // fermer le test apres l'utulisation
    afterAll((done)=> {
        app.close(done)
    })
    it('new event', (done) => {
        request(app)
            .post('/new-evenement')
            .send({
                nom : 'BU',
                createur: 'sonia',
                dateDebut: '2525-01-01',
                dateFin: '2525-02-01'
            })
            .expect(302)
            .end((err, res) => {
                if (err) {
                    return done(err)
                }
                done()
            })
    })
    it('event sup', (done) => {
        request(app)
            .get('/evenement-sup/8a4f5990-52bf-11ec-8f61-893c2a269c0b')
            .expect(302)
            .end((err, res) => {
                if (err) {
                    return done(err)
                }
                done()
            })
    })

    it('event edit', (done) => {
        request(app)
            .post('/evenement-modif/8a4f5990-52bf-11ec-8f61-893c2a269c0b')
            .send({
                nom : 'BU',
                createur: 'sonia',
                dateDebut: '2525-01-01',
                dateFin: '2525-02-01'
            })
            .expect(302)
            .end((err, res) => {
                if (err) {
                    return done(err)
                }
                done()
            })
    })
})
