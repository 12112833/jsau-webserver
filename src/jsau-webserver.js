'use strict'
const express = require('express')
const app = express()
const morgan = require('morgan')
const bunyan = require('bunyan')
const bodyParser = require('body-parser')
const log = bunyan.createLogger({name: 'jsau-webserver'})
const nunjucks = require('nunjucks')
const fs = require('fs')
const path = require('path')
const {
    v1: uuidv1,
} = require('uuid')

app.use(morgan('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
const port = 8082

//app.use(express.static(path.join(__dirname, 'views')))

nunjucks.configure('src/views', {
    autoescape: true,
    express: app
})
const filePath = './src/data/evenements.json'

app.get('/', (req, res) => {
    fs.promises.readFile(filePath, 'utf8')
        .then(JSON.parse)
        .then((obj)=>{
            res.render('evenement.html', {events: obj})
        })
        .catch((err)=>{
            log.info('Invalis JSON in file :', err)
            res.send(err.message)
        })
})

app.get('/evenement/creat', (req, res) => {

    res.render('creat.html')
})

app.post('/new-evenement', async(req, res) => {

    try {
        req.body.id = uuidv1()
        const content = req.body
        let data = await fs.promises.readFile(filePath, 'utf8')
        data = await JSON.parse(data)
        data.push(content)
        await fs.promises.mkdir(path.dirname(filePath), {recursive: true})
        await fs.promises.writeFile(filePath, JSON.stringify(data, null, 4), {flag:'w'})
        res.redirect('/')
    } catch (err) {
        log.error('Fail to write in file : ', err)
        res.send('Fail to write in file')
    }
})
app.get('/evenement-sup/:id', async(req, res) => {

    try {

        const deleteData = await fs.promises.readFile(filePath, 'utf8')
        const obj = await JSON.parse(deleteData)
        obj.splice(obj.findIndex(item => item.id === req.params.id), 1)
        await fs.promises.mkdir(path.dirname(filePath), {recursive: true})
        await fs.promises.writeFile(filePath, JSON.stringify(obj, null, 4), {flag:'w'})
        res.redirect('/')
    } catch (err) {
        log.error('Fail to write in file : ', err)
        res.send('Fail to write in file')
    }
})

app.post('/evenement-modif/:id', async(req, res) => {

    try {
        req.body.id = req.params.id
        const content = req.body
        const data = await fs.promises.readFile(filePath, 'utf8')
        const obj = await JSON.parse(data)
        obj[obj.findIndex(item => item.id === req.params.id)] = content
        await fs.promises.mkdir(path.dirname(filePath), {recursive: true})
        await fs.promises.writeFile(filePath, JSON.stringify(obj, null, 4), {flag:'w'})
        await res.redirect('/')
    } catch (err) {
        log.error('Fail to write in file : ', err)
        res.send('Fail to write in file')
    }
})
app.get('/evenement/edit/:id', async(req, res) => {

    try {
        let data = await fs.promises.readFile(filePath, 'utf8')
        data = await JSON.parse(data)
        data = await data.find(item => item.id === req.params.id)
        res.render('edit.html', {evenement_id: data})
    } catch (err) {
        log.error('Fail to write in file : ', err)
        res.send('Fail to write in file')
    }
})

module.exports = app.listen(port, () => {
    log.info(`Example app listening at http://localhost:${port}`)
})